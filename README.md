# CoCoWin

This project automates the setup and usage of WSL as a build engine for CoCo development. It...
- Installs updates on a base Ubuntu WSL distribution and installs toolshed, lwtools, and cmoc
- Downloads and installs the VCC and MAME emulators in the current directory, including the CoCo2 and CoCo3 roms for MAME
- Automates building source files, copying to disk, and launching an emulator

Note: This is in development and currently doesn't check to see if a build succeeds before launching the emulator. If a cmoc build fails, it currently proceeds to launch the emulator with the last successful build.

## Requirements
- [WSL](https://learn.microsoft.com/en-us/windows/wsl/about)
- [7zip](https://7ziphelp.com/7-zip-for-pc) (For extracting emulators)

## WSL Setup
WSL is "Windows Subsystem for Linux." It allows you to run a linux distribution inside Windows. This project assumes that you already have WSL installed and working on Windows. The "wsl --status" command should show that it is running:
```
C:\temp\cocowin>wsl --status
Default Distribution: Ubuntu
Default Version: 2

Windows Subsystem for Linux was last updated on 3/25/2022
WSL automatic updates are on.

Kernel version: 5.10.102.1
```
This project will use the Ubuntu distribution and includes a .bat file that will install updates and download and build the latest versions of toolshed, lwtools, and CMOC.

(Warning: This step could be dangerous if you alredy have WSL distributions installed that are important.) To install a fresh copy of the WSL Ubuntu distribution, use "wsl --install -d Ubuntu". After it completes, you will be asked for a default username and password for Ubuntu. 

```
C:\temp\cocowin>wsl --install -d Ubuntu
Ubuntu is already installed.
Launching Ubuntu...
```
This projects expects this Ubuntu distro to be the default in WSL, so we can set it to default like this:

```
C:\temp\cocowin>wsl --set-default Ubuntu
```

To see the the WSL distributions you currently have installed, use "wsl --list"
```
C:\temp\cocowin>wsl --list
Windows Subsystem for Linux Distributions:
Ubuntu (Default)
Debian
Ubuntu-20.04
```
Now, we can run the "setup_ubuntu.bat" file to install updates and download, build, and install toolshed, lwtools, and cmoc:
```
C:\temp\cocowin>setup_ubuntu.bat
[sudo] password for jpittman:
Get:1 http://security.ubuntu.com/ubuntu jammy-security InRelease [110 kB]
Hit:2 http://archive.ubuntu.com/ubuntu jammy InRelease
Get:3 http://security.ubuntu.com/ubuntu jammy-security/main amd64 Packages [530 kB]
Get:4 http://archive.ubuntu.com/ubuntu jammy-updates InRelease [114 kB]
...
```
## Emulators
The "download_emulators.bat" script will download The VCC and MAME emulators (and coco2 and coco3 roms) and place them in the "Emulators" directory. 
```
c:\temp\cocowin>download_emulators.bat
--2022-12-15 09:59:50--  https://github.com/VCCE/VCC/releases/download/vcc2.1.0.7/VCC.2.1.0.7.Coco.3.Emulator.zip
Resolving github.com (github.com)... 140.82.114.3
Connecting to github.com (github.com)|140.82.114.3|:443... connected.
HTTP request sent, awaiting response... 302 Found
...
```
## Running code in emulators
The coco.bat script will handle creating and mounting a disk file, compiling c code using cmoc, sending .bas code or built binaries to the disk file, and launching an emulator. The basic usage is:
```
C:\temp\cocowin>coco (sourcefile) (diskfile) [options]
```
For example, this command should add the included sample BASIC file to "coco.dsk" and launch MAME with it available on the floppy drive:
```
C:\temp\cocowin>coco test.bas coco.dsk
```
C source works just the same:
```
C:\temp\cocowin>coco ctest.c coco.dsk
```
You can even automatically load and run the program in the emulator
```
C:\temp\cocowin>coco ctest.c coco.dsk --autorun
```
For a quick look at the effects of code changes, the "--fast" flag will run MAME without throttling the cpu to CoCo speed:
```
C:\temp\cocowin>coco ctest.c coco.dsk --autorun --fast
```
## Supported Flags
```
--vcc:              Launches the VCC emulator with the disk mounted
--recreate_dsk:     Rebuilds the .dsk file. (By default, files are only added and a new disk is not created)
* The flags below only work with MAME
--fast:             Launches mame with --nothrottle flag so host CPU is not throttled to CoCo speed
--autoload:         Types "LOAD..." or "LOADM..." on launch
--autorun:          Type "RUN..." or "LOADM...EXEC" on launch
--coco2:            Launches MAME with the coco2 emulator (Default is coco3)
```
## Flags to be implemented
```
--dir:              Specifies a different working directory
--all:              Builds and/or copies all source files in the directory to .dsk
```
