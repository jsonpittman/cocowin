@ECHO OFF

set vcc_url=https://github.com/VCCE/VCC/releases/download/vcc2.1.0.7/VCC.2.1.0.7.Coco.3.Emulator.zip
set mame_url=https://github.com/mamedev/mame/releases/download/mame0250/mame0250b_64bit.exe
set coco2_rom=https://colorcomputerarchive.com/repo/ROMs/MAME-MESS/coco2.zip
set coco3_rom=https://colorcomputerarchive.com/repo/ROMs/MAME-MESS/coco3.zip

set filename=
set extractdir=

CALL :DownloadFile %vcc_url%

set extractdir=VCC
CALL :ExtractFile %filename%

CALL :DownloadFile %mame_url%
set extractdir=MAME
CALL :ExtractFile %filename%

CALL :DownloadFile %coco2_rom%
CALL :CopyROM

CALL :DownloadFile %coco3_rom%
CALL :CopyROM

echo "Done!"
GOTO :EOF

:CopyROM
IF NOT EXIST Emulators\MAME\roms\%filename% (
    copy Downloads\%filename% Emulators\MAME\roms\%filename%
    echo ROM copied: %filename%
) ELSE echo ROM exists: %filename%
exit /b

:DownloadFile
CALL :GetFilename %1
IF NOT Exist Downloads\ (
    mkdir Downloads
)

IF NOT EXIST Downloads\%filename% (
    cd Downloads
    wget %1
    cd ..
) ELSE echo %filename% already exists.
exit /b

:ExtractFile
IF NOT EXIST Emulators\%extractdir% (
    7z x -y Downloads\%filename% -oEmulators\%extractdir%\
) ELSE echo %extractdir% directory already exists.
exit /b

:GetFilename
set filename=%~nx1
exit /b