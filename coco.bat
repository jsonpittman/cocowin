:: coco.bat
::
:: Usage: coco (source filename) (dsk file) [-flag1 -flag2...]
:: Example: coco ctest.c coco.dsk
::
::
:: Flags:
::   	--recreate_dsk: if present, deletes and recreates dsk file on each build. 
::			If not present, dsk file is appended on each build 
::		--vcc: Launches the VCC emulator (instead of MAME)
::		--autoload: automatically loads code in emulator
::		--autorun: automatically loads code in emulator & runs it
::		--fast: (MAME only). Executes code really fast for quick view of changes
::TODO:
::		--dir: specify a different directory 
::		--all: copy (BASIC) or compile (c) all files in current (or specified) dir
::		--coco2: (MAME only). Launches MAME CoCo2 emulator
::

@ECHO OFF

rem Declare Variables
set emu_dir=Emulators\mame\  
set emu_filename=mame
set speed_flag=
set recreate_dsk=N
set autoload=N
set autorun=N
set mame_flags=
set launch_cmd=
set load_cmd=
set rom_name=coco3

FOR %%a IN (%*) DO (
  IF /I "%%a"=="--vcc" (
  	SET emu_dir=Emulators\VCC\
	set emu_filename=vcc
  ) 
  IF /I "%%a"=="--fast" SET speed_flag=-nothrottle
  IF /I "%%a"=="--recreate_dsk" SET recreate_dsk=Y
  IF /I "%%a"=="--autoload" SET autoload=Y
  IF /I "%%a"=="--autorun" SET autorun=Y
  IF /I "%%a"=="--coco2" SET rom_name=coco2
)

set current_dir=%CD%
set folder=%current_dir%
set wsl_folder=%folder:c:\=/mnt/c/%
set wsl_folder=%wsl_folder:\=/%
for /F %%i in ("%1") do set "base=%%~ni"
CALL :Uppercase %base%
set base_upper=%ucase%
for %%i in ("%1") do set "exten=%%~xi"
CALL :Uppercase %exten%
set exten_upper=%ucase%

IF "%exten_upper%"==".C" (
	echo c source file detected!
	set decb_param=-2 -b
	set launch_exten=.BIN
	IF "%autoload%"=="Y" (
		echo Autoload...
		set load_cmd=LOADM
		set launch_cmd=
	)
	IF "%autorun%"=="Y" (
		echo Autorun...
		set load_cmd=LOADM
		set launch_cmd=:EXEC
	)
)

IF "%exten_upper%"==".BAS" (
	echo BASIC file detected!
	set decb_param=-1 -a -t
	IF "%autoload%"=="Y" (
		echo Autoload...
		set load_cmd=LOAD
		set launch_cmd=
	)
	IF "%autorun%"=="Y" (
		echo Autorun...
		set load_cmd=RUN
		set launch_cmd=
	)
    
	set launch_exten=.BAS
)

IF %exten_upper%==.C (
	echo Compiling: %1
	bash -c "cmoc -o %wsl_folder%/%base%.bin %wsl_folder%/%1"
)

IF "%autoload%"=="Y" (
    set mame_flags= -autoboot_delay 1 -autoboot_command %load_cmd%\"%base_upper%%launch_exten%\"%launch_cmd%\n
)
IF "%autorun%"=="Y" (
    set mame_flags= -autoboot_delay 1 -autoboot_command %load_cmd%\"%base_upper%%launch_exten%\"%launch_cmd%\n
)

@REM GOTO :EOF

for %%i in ("%1") do set "exten=%%~xi"
CALL :Uppercase %exten%
set exten_upper=%ucase%

rem create dsk if it doesn't exist
IF NOT EXIST %2 (
	echo Creating Disk: %2
	bash -c "decb dskini %wsl_folder%/%2"
)

echo Copying to DSK: %base_upper%%launch_exten%
bash -c "decb copy %decb_param% -b %base%%launch_exten% -r %2,%base_upper%%launch_exten%"
cd %emu_dir%
echo Launching %emu_filename%...

if "%emu_filename%"=="vcc" (
	%emu_filename%
)else (
	%emu_filename% -video gdi %rom_name% -skip_gameinfo -window -flop1 "%folder%\%2" -mouse_device none %speed_flag% %mame_flags%
)
cd %current_dir%
GOTO :EOF

:LaunchExecutables
if %recreate_dsk%==Y (
	echo Recreating Disk...
	del %2
)

EDIT /B 0

:Uppercase
SET ucase=%~1
SET ucase=%ucase:a=A%
SET ucase=%ucase:b=B%
SET ucase=%ucase:c=C%
SET ucase=%ucase:d=D%
SET ucase=%ucase:e=E%
SET ucase=%ucase:f=F%
SET ucase=%ucase:g=G%
SET ucase=%ucase:h=H%
SET ucase=%ucase:i=I%
SET ucase=%ucase:j=J%
SET ucase=%ucase:k=K%
SET ucase=%ucase:l=L%
SET ucase=%ucase:m=M%
SET ucase=%ucase:n=N%
SET ucase=%ucase:o=O%
SET ucase=%ucase:p=P%
SET ucase=%ucase:q=Q%
SET ucase=%ucase:r=R%
SET ucase=%ucase:s=S%
SET ucase=%ucase:t=T%
SET ucase=%ucase:u=U%
SET ucase=%ucase:v=V%
SET ucase=%ucase:w=W%
SET ucase=%ucase:x=X%
SET ucase=%ucase:y=Y%
SET ucase=%ucase:z=Z%
EXIT /b
