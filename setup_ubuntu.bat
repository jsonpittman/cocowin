@ECHO OFF
set toolshed_url=https://sourceforge.net/projects/toolshed/files/ToolShed/ToolShed\ 2.2/toolshed-2.2.tar.gz
set lwtools_url=http://www.lwtools.ca/releases/lwtools/lwtools-4.20.tar.gz
set cmoc_url=http://perso.b2b2c.ca/~sarrazip/dev/cmoc-0.1.80.tar.gz

bash -c "sudo apt update && sudo apt upgrade -y && sudo apt install make flex bison build-essential markdown libfuse-dev -y && sudo apt autoremove && mkdir ~/Downloads && cd ~/Downloads && wget %toolshed_url% && sudo tar -zxvf toolshed-2.2.tar.gz && cd toolshed-2.2/build/unix && sudo make && sudo make install && cd ~/Downloads && wget %lwtools_url% && tar -zxvf lwtools-4.20.tar.gz && cd lwtools-4.20 && sudo make && sudo make install && cd ~/Downloads && wget %cmoc_url% && tar -zxvf cmoc-0.1.80.tar.gz && cd cmoc-0.1.80 && ./configure && sudo make && sudo make install"
:EOF